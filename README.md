## Project Squad

Project created in free time

## Movement in active view (by mouse position)
[W] - forward\
[S] - backward\
[A] - step left\
[D] - step right\
[Q] - turn left\
[E] - turn right

## Actions
[Left mouse button] - fire\
[Space] - slow time\
[F5] - reload scene
