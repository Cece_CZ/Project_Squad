using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamage
{
    DamageType GetDamageType
    {
        get;
    }

    float GetAmount
    {
        get;
    }
}
