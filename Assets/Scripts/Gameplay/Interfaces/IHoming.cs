using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHoming
{
    void SetTarget(Transform trTarget);
}
