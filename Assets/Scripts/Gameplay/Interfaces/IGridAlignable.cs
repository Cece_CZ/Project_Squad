﻿namespace Gameplay.Interfaces
{
    public interface IGridAlignable
    {
        void DoGridAlign();
    }
}