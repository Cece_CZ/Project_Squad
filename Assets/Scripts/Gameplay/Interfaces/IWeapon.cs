using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IWeapon 
{
    void SetTargeting(Ray ray);
        
    void StartShooting();

    void StopShooting();
}
