using UnityEngine;

// TODO: 2024-05-13 Maybe rename to deactivatable or something
public interface IDestroyable
{
    void Destroy();
    
    // TODO: 2024-05-13 Add Reactive() - to get back to initial state

    void FinalBlow(Vector3 position); // TODO: 2024-05-13 Create a special interface just for this function
}
