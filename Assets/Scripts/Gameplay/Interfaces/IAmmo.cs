using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAmmo
{
    float GetSpeed();

    void Activate(Vector3 pos, Quaternion rot);

    void Deactivate();
}
