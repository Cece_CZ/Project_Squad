using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamageReceiver
{
    float GetHealth
    {
        get;
    }

    void SetManager(IDamageManager manager);

    void ReceiveDamage(IDamage damage, Vector3 position);

    void StopWorking();
}
