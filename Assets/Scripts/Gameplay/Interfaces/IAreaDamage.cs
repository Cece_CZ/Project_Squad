using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAreaDamage
{
    float Range
    {
        get;
    }
}
