using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDamageManager 
{
    void DamageChanged(Vector3 position);
}
