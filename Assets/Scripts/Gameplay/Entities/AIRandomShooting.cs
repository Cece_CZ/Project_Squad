using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIRandomShooting : MonoBehaviour, IDestroyable
{
    // References
    private IWeapon weapon;

    // Settings
    public float MinDelay = 2;
    public float MaxDelay = 5;
    public float FireingLength = 1;

    private void Awake()
    {
        this.weapon = GetComponentInChildren<IWeapon>();

        StartCoroutine(Shooting());
    }

    IEnumerator Shooting()
    {
        yield return new WaitForSeconds(Mathf.Lerp(this.MinDelay, this.MaxDelay, Random.value));

        this.weapon.StartShooting();

        yield return new WaitForSeconds(this.FireingLength);

        this.weapon.StopShooting();

        StartCoroutine(Shooting());
    }

    public void FinalBlow(Vector3 position)
    {

    }

    public void Destroy()
    {
        StopAllCoroutines();

        this.weapon.StopShooting();
    }
}
