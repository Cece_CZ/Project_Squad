using Gameplay.Interfaces;
using UnityEngine;

public class Obstacle : MonoBehaviour, IGridAlignable
{
    public void DoGridAlign()
    {
        Utils.Scene.Functions.GridAlign(this.transform);
    }
}
