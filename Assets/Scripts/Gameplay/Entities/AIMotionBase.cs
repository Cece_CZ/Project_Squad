﻿using UnityEngine;

namespace Gameplay.Entities
{
    public abstract class AIMotionBase : MonoBehaviour, IDestroyable
    {
        // References
        protected EntityMotion motion;

        // State
        protected bool isWorking = true;
        
        protected void Awake()
        {
            this.motion = GetComponent<EntityMotion>();
            
            this.motion.OnMotionFinished = StepFinished;
        }

        protected abstract void DoStep();

        private void Start()
        {
            DoStep();
        }

        public void Destroy()
        {
            this.isWorking = false;
        }

        public void FinalBlow(Vector3 position)
        {
        }
        
        private void StepFinished()
        {
            DoStep();
        }
    }
}