using Random = UnityEngine.Random;

namespace Gameplay.Entities
{
    public class AIRandomMotion : AIMotionBase
    {
        protected override void DoStep()
        {
            while (true)
            {
                if (!isWorking)
                {
                    return;
                }

                int random = (int)(Random.value * 4.9999);
                bool result = false;

                switch (random)
                {
                    case 0:
                        result = this.motion.StepForward();
                        break;
                    case 1:
                        result = this.motion.StepForward();
                        break;
                    case 2:
                        result = this.motion.StepForward();
                        break;
                    case 3:
                        result = this.motion.StepForward();
                        break;
                    case 4:
                        result = this.motion.Rotate90Left();
                        break;
                    case 5:
                        result = this.motion.Rotate90Right();
                        break;

                    default:
                        break;
                }

                if (result == false)
                {
                    continue;
                }

                break;
            }
        }
    }
}
