using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayersRobot : MonoBehaviour, IDestroyable
{
    // References
    public UI.Robot uiRobot;
    [HideInInspector] public EntityMotion Entity;
    [HideInInspector] public Camera Camera;
    public IWeapon Weapon;

    // State
    public bool IsControlActive = false;

    private void Awake()
    {
        this.Entity = GetComponent<EntityMotion>();
        this.Camera = GetComponentInChildren<Camera>();
        this.Weapon = GetComponentInChildren<IWeapon>();
    }

    public void FinalBlow(Vector3 position)
    {
    }

    public void Destroy()
    {
        this.uiRobot.ShowDestroy();
    }
}
