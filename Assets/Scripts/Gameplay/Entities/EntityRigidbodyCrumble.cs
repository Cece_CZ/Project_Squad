using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityRigidbodyCrumble : MonoBehaviour, IDestroyable
{
    // References
    private Rigidbody[] rigidbodies = null;
    private int rigidbodiesCount;

    void Awake()
    {
        this.rigidbodies = GetComponentsInChildren<Rigidbody>();
        this.rigidbodiesCount = this.rigidbodies.Length;
    }

    public void Destroy()
    {
        for (int i = 0; i < this.rigidbodiesCount; i++)
        {
            this.rigidbodies[i].isKinematic = false;
        }
    }

    public void FinalBlow(Vector3 position)
    {
        for (int i = 0; i < this.rigidbodiesCount; i++)
        {
            this.rigidbodies[i].AddExplosionForce(10, position, 50, 0, ForceMode.Impulse);
        }
    }
}
