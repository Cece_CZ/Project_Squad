using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageReceiver : MonoBehaviour, IDamageReceiver
{
    // Refences
    private IDamageManager damageManager;
    private Collider[] collisions;
    private int collisionsCount;

    // State
    public float Health = 100;
    public float GetHealth => Health;

    void Awake()
    {
        this.collisions = Utils.GameObjects.Functions.GetCollidersInLayer(this.gameObject, "Enemies", "Players");
        this.collisionsCount = this.collisions.Length;
    }

    public void SetManager(IDamageManager manager)
    {
        this.damageManager = manager;
    }

    public void ReceiveDamage(IDamage damage, Vector3 position)
    {
        this.Health -= damage.GetAmount;

        this.damageManager.DamageChanged(position);
    }

    public void StopWorking()
    {
        for (int i = 0; i < this.collisionsCount; i++)
        {
            this.collisions[i].enabled = false;
        }
    }
}
