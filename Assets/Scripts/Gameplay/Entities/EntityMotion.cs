using System.Collections;
using System.Collections.Generic;
using Gameplay.Interfaces;
using UnityEngine;
using UnityEngine.Events;

public class EntityMotion : MonoBehaviour, IDestroyable, IGridAlignable
{
    // Reference
    public MotionSettings PlayersRobotMotion;

    public UnityAction OnMotionFinished;
    
    // Caches
    private Transform trCache;
    private Transform trDynamicMotionObstacle;

    // Settings
    private int motionObstaclesLayers;
    private Vector3 motionCheckExtent = new Vector3(0.2f, 0.2f, 0.2f);

    // State
    private Vector3 motionTargetPosition = Vector3.zero;
    private bool isMoving = false;
    private bool isWorking = true;
    
    private void Awake()
    {
        this.trCache = this.transform;

        // Obstacles
        this.motionObstaclesLayers = LayerMask.GetMask("WorldObstacles", "DynamicObstacles");

        // Find collider in DynamicObstacles layer
        this.trDynamicMotionObstacle = Utils.GameObjects.Functions.GetColliderInLayer(this.gameObject, "DynamicObstacles").transform;
        this.trDynamicMotionObstacle.SetParent(null);
    }

    private void TryToMove(Vector3 targetPosition)
    {
        if (this.isMoving) return;
        
        this.motionTargetPosition = targetPosition;
        this.motionTargetPosition.y += 0.5f;

        if (Physics.CheckBox(this.motionTargetPosition, motionCheckExtent, Quaternion.identity, this.motionObstaclesLayers))
        {
            return;
        }
            
        this.trDynamicMotionObstacle.position = targetPosition;
        this.isMoving = true;
        StartCoroutine(Move(targetPosition));
    }

    public bool StepForward()
    {
        if (!isWorking)
        {
            return false;
        }

        Vector3 targetPos = this.transform.position + this.transform.forward * GameSettings.Instance.GridSize;
        this.TryToMove(targetPos);
        
        return this.isMoving;
    }

    public bool StepBackward()
    {
        if (!isWorking)
        {
            return false;
        }

        Vector3 targetPos = this.transform.position - this.transform.forward * GameSettings.Instance.GridSize;
        this.TryToMove(targetPos);
        
        return this.isMoving;
    }

    public bool StepLeft()
    {
        if (!isWorking)
        {
            return false;
        }

        Vector3 targetPos = this.transform.position - this.transform.right * GameSettings.Instance.GridSize;
        this.TryToMove(targetPos);
        
        return this.isMoving;
    }

    public bool StepRight()
    {
        if (!isWorking)
        {
            return true;
        }

        Vector3 targetPos = this.transform.position + this.transform.right * GameSettings.Instance.GridSize;
        this.TryToMove(targetPos);
        
        return this.isMoving;
    }

    public bool Rotate90Left()
    {
        if (!isWorking)
        {
            return false;
        }

        Quaternion rotation = this.transform.rotation;
        rotation *= Quaternion.Euler(0, -90, 0);

        if (!this.isMoving)
        {
            this.isMoving = true;
            StartCoroutine(Rotate(rotation));
        }

        return this.isMoving;
    }

    public bool Rotate90Right()
    {
        if (!isWorking)
        {
            return false;
        }

        Quaternion rotation = this.transform.rotation;
        rotation *= Quaternion.Euler(0, 90, 0);

        if (!this.isMoving)
        {
            this.isMoving = true;
            StartCoroutine(Rotate(rotation));
        }
        
        return this.isMoving;
    }

    public bool IsTargetPositionFree(Vector3 targetPosition)
    {
        this.motionTargetPosition = targetPosition;
        this.motionTargetPosition.y += 0.5f;

        return !Physics.CheckBox(this.motionTargetPosition, motionCheckExtent, Quaternion.identity, this.motionObstaclesLayers);
    }

    IEnumerator Move(Vector3 targetPosition)
    {
        Vector3 pos = this.trCache.position;

        float time = Time.time;
        float state;

        while ((state = this.PlayersRobotMotion.StepCurve.Evaluate(Time.time - time)) < 1)
        {
            if (!isWorking)
            {
                yield break;
            }

            this.trCache.position = Vector3.Lerp(pos, targetPosition, state);
            yield return null;
        }

        this.trCache.position = targetPosition;

        this.isMoving = false;

        if (OnMotionFinished != null)
        {
            OnMotionFinished();
        }
    }

    IEnumerator Rotate(Quaternion targetRotation)
    {
        Quaternion rot = this.trCache.rotation;

        float time = Time.time;
        float state;

        while ((state = this.PlayersRobotMotion.RotateCurve.Evaluate(Time.time - time)) < 1)
        {
            if (!isWorking)
            {
                yield break;
            }

            this.trCache.rotation = Quaternion.Lerp(rot, targetRotation, state);
            yield return null;
        }

        this.trCache.rotation = targetRotation;

        this.isMoving = false;
        
        if (OnMotionFinished != null)
        {
            OnMotionFinished();
        }
    }

    public void Destroy()
    {
        this.trDynamicMotionObstacle.gameObject.SetActive(false);
       
        this.isWorking = false;
    }

    public void FinalBlow(Vector3 position)
    {

    }

    public void DoGridAlign()
    {
        Utils.Scene.Functions.GridAlign(this.transform);
    }
}
