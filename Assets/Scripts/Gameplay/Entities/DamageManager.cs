using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageManager : MonoBehaviour, IDamageManager
{
    // References
    [SerializeField] private GameObject ui = null;

    private IDamageReceiver[] damageReceivers = null;
    private int damageReceiversCount = 0;
    private IDestroyable [] destroyables = null;
    private int destroyablesCount = 0;
    private IHealthBar[] healthBars = null;
    private int healthBarsCount = 0;

    // Debug
    //private string stringTotalHealth = "0";

    // State
    private float totalHealth = 0;

    void Awake()
    {
        this.damageReceivers = GetComponentsInChildren<IDamageReceiver>();
        this.damageReceiversCount = damageReceivers.Length;

        this.destroyables = GetComponentsInChildren<IDestroyable>();
        this.destroyablesCount = destroyables.Length;

        this.totalHealth = 0;
        for (int i = 0; i < this.damageReceiversCount; i++)
        {
            this.damageReceivers[i].SetManager(this);
            this.totalHealth += this.damageReceivers[i].GetHealth;
        }

        //this.stringTotalHealth = this.totalHealth.ToString();

        if (ui != null)
        {
            this.healthBars = ui.GetComponentsInChildren<IHealthBar>();
            this.healthBarsCount = healthBars.Length;
        }
    }

    //void Update()
    //{
    //    Debug.DrawLine(transform.position, Vector3.zero);
    //}

    public void DamageChanged(Vector3 position)
    {
        float total = 0;

        for (int i = 0; i < this.damageReceiversCount; i++)
        {
            total += this.damageReceivers[i].GetHealth;
        }

        //this.stringTotalHealth = total.ToString();

        if(total < 0)
        {
            total = 0;
        }

        float state = total / this.totalHealth;
        for (int i = 0; i < this.healthBarsCount; i++)
        {
            this.healthBars[i].Set(state);
        }

        if(total > 0)
        {
            return;
        }

        ///////////////////////////////////////////////////////////////////////
        // If reach this position it is end
        for (int i = 0; i < this.damageReceiversCount; i++)
        {
            this.damageReceivers[i].StopWorking();
        }

        for (int i = 0; i < this.destroyablesCount; i++)
        {
            IDestroyable destroyable = this.destroyables[i];
            destroyable.Destroy();
            destroyable.FinalBlow(position);
        }
    }

    //private void OnGUI()
    //{
    //    if (this.stringTotalHealth != "0" && !this.stringTotalHealth.Contains("-"))
    //    {
    //        var cameras = Camera.allCameras;
    //        int count = cameras.Length;

    //        for (int i = 0; i < count; i++)
    //        {
    //            Vector3 position = this.transform.position;
    //            position.y += 1.2f;
    //            Vector3 posOnScreen = cameras[i].WorldToScreenPoint(position);
    //            Rect r = new Rect(posOnScreen.x, Screen.height - posOnScreen.y, 300, 300);

    //            GUI.color = Color.red;
    //            GUI.Label(r, this.stringTotalHealth);
    //        }
    //    }
    //}
}
