﻿using UnityEngine;
using Utils;

namespace Gameplay.Entities
{
    public class AIWaypointsMotion : AIMotionBase
    {
        public MovingPath Waypoints;

        public bool Rotate = true;
        
        public bool LoopPath;
        
        protected override void DoStep()
        {
            float gridSize = GameSettings.Instance.GridSize;
            Vector3 currentPosition = this.transform.position;
            Vector3 targetPosition = Polylines.GetNextPointFromClosestPoint(
                Waypoints.Waypoints,
                currentPosition,
                gridSize,
                LoopPath);

            Vector3 directionToTarget = (targetPosition - currentPosition).normalized;
            float angleToTarget = Vector3.SignedAngle(transform.forward, directionToTarget, Vector3.up);

            if (!LoopPath && directionToTarget.sqrMagnitude < 0.01f)
            {
                return;
            }

            if (Rotate)
            {
                switch (angleToTarget)
                {
                    case > -45 and < 45:
                        this.motion.StepForward();
                        break;
                    case >= 45 and <= 180:
                        this.motion.Rotate90Right();
                        break;
                    default:
                        this.motion.Rotate90Left();
                        break;
                }
            }
            else
            {
                switch (angleToTarget)
                {
                    case > -45 and < 45:
                        this.motion.StepForward();
                        break;
                    case >= 45 and <= 135:
                        this.motion.StepRight();
                        break;
                    case >= -135 and <= -45:
                        this.motion.StepLeft();
                        break;
                    default:
                        this.motion.StepBackward();
                        break;
                }
            }
        }
    }
}