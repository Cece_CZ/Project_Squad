﻿using System;
using UnityEngine;

namespace Gameplay.Weapons
{
    public class Weapon : MonoBehaviour, IWeapon, IDestroyable
    {
        // Settings
        public LayerMask ValidTargets;
        protected int TargetsLayers;
        
        // Caches
        protected Transform TrOfThisCache;
        protected Transform TrFirePoint;
        
        // References
        protected IAmmo[] Projectiles;
        protected int ProjectilesCount;
        
        // State
        protected bool isDestroyed = false;
        
        protected virtual void Awake()
        {
            // Cannot add as default, so add here
            if (this.ValidTargets.value == 0)
            {
                this.ValidTargets = LayerMask.GetMask("WorldObstacles", "Enemies", "Players");
            }
            this.TargetsLayers = ValidTargets.value;
            
            this.TrOfThisCache = transform;
            this.TrFirePoint = GetComponentInChildren<WeaponsFirePoint>()?.transform;
            
            this.Projectiles = GetComponentsInChildren<IAmmo>(true);
            this.ProjectilesCount = Projectiles.Length;
        }

        public virtual void SetTargeting(Ray ray)
        {
        }

        public virtual void StartShooting()
        {
        }

        public virtual void StopShooting()
        {
        }

        public virtual void Destroy()
        {
            this.isDestroyed = true;
        }

        public virtual void FinalBlow(Vector3 position)
        {
        }
    }
}