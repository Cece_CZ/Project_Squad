using UnityEngine;

namespace Gameplay.Weapons.Ammo
{
    public class Rocket : Ammo, IHoming, IAreaDamage
    {
        // Basics
        public float Range = 5;
        public float Speed = 1;
        public float AngleChangeSpeed = 60;

        // Caches
        [SerializeField]
        private ParticleSystem[] detonations;
        private int detonationsCount;

        // Working values
        private Ray ray = new Ray(); 
        private Vector3 curentDirection;
        private Transform trTarget = null;

        // Layers TODO: make some global layers
        private int targetsLayers;
        private int enemyLayers;
        private int obstaclesLayers;
        
        float IAreaDamage.Range => this.Range;

        protected override void Awake()
        {
            base.Awake();
            
            this.detonationsCount = this.detonations.Length;
            this.targetsLayers = LayerMask.GetMask("WorldObstacles", "Enemies", "Players");
            this.enemyLayers = LayerMask.GetMask("Enemies", "Players");
            this.obstaclesLayers = LayerMask.GetMask("WorldObstacles");
        }
        
        public override float GetSpeed()
        {
            return Speed;
        }

        public override void Activate(Vector3 pos, Quaternion rot)
        {
            if (!this.gameObject.activeSelf)
            {
                this.gameObject.SetActive(true);
            }

            if (this.TrOfThisCache.parent != null)
            {
                this.TrOfThisCache.SetParent(null);
            }

            this.TrOfThisCache.position = pos;
            this.TrOfThisCache.rotation = rot;

            this.curentDirection = rot * Vector3.forward;
            //this.timeOfBirth = Time.time;

            this.ray.origin = pos;
        }

        public override void Deactivate()
        {
        
        }

        public void SetTarget(Transform trTarget)
        {
            this.trTarget = trTarget;
        }
        
        void Update()
        {
            float deltaTime = Time.deltaTime;

            // Moving toward Target
            Vector3 toTarget = this.trTarget.position - this.TrOfThisCache.position;
            this.curentDirection = Vector3.RotateTowards(this.curentDirection, toTarget, this.AngleChangeSpeed * deltaTime * Mathf.Deg2Rad, 0).normalized;

            Debug.DrawLine(this.TrOfThisCache.position, this.trTarget.position);

            // Move
            Vector3 pos = this.ray.origin;
            pos += Time.deltaTime * this.Speed * this.curentDirection;

            Quaternion rot = Quaternion.LookRotation(this.curentDirection, Vector3.up);

            this.TrOfThisCache.SetPositionAndRotation(pos, rot);

            // Collision detection
            Vector3 dir = pos - this.ray.origin;
            this.ray.direction = dir;
            var hits = Physics.RaycastAll(this.ray, dir.magnitude, targetsLayers);
            int count = hits.Length;

            if (count > 0)
            {
                // If rocket explodes...
                RaycastHit hit = Utils.Physics.GetClosestHit(hits);

                // Check everything in range
                var surroundingColliders = Physics.OverlapSphere(hit.point, this.Range, this.enemyLayers);

                // Check obstacles
                foreach (var surroundingCollider in surroundingColliders)
                {
                    // Ray from impact point to center of collider
                    this.ray.direction = surroundingCollider.bounds.center - pos;
                    var hitsFromBlast = Physics.RaycastAll(this.ray, this.ray.direction.magnitude, this.obstaclesLayers);

                    // There is obstacles so no hit
                    if(hitsFromBlast.Length > 0)
                    {
                        RaycastHit firstHit = Utils.Physics.GetClosestHit(hitsFromBlast);

                        Debug.DrawLine(pos, firstHit.point);
                    }
                    else // Do damage
                    {
                        IDamageReceiver damageReceiver = surroundingCollider.GetComponent<IDamageReceiver>();
                        if (damageReceiver != null)
                        { // Deal damage
                            damageReceiver.ReceiveDamage(this.Damage, hit.point);
                        }
                        else
                        { // Default effect

                        }

                        Debug.DrawLine(pos, surroundingCollider.bounds.center);
                    }
                }
                //Debug.Break();
                // Disable projectile
                this.gameObject.SetActive(false);

                // Play detonation effect
                for (int i = 0; i < this.detonationsCount; i++)
                {
                    ParticleSystem ps = this.detonations[i];
                    ps.transform.SetParent(null);
                    ps.transform.position = hit.point;
                    ps.Play();
                }
            }

            // Last pos
            this.ray.origin = pos;
        }
    }
}
