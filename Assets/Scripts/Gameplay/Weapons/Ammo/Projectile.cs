﻿using UnityEngine;

namespace Gameplay.Weapons.Ammo
{
    public class Projectile : Ammo
    {
        // Settings
        public float Speed = 1;
        public float Lifetime = 10;

        // State
        private float timeOfDeactivate;
        private Ray ray = new Ray();

        int targetsLayers;

        [SerializeField]
        private ParticleSystem[] detonations;
        private int detonationsCount;

        protected override void Awake()
        {
            base.Awake();
            
            this.targetsLayers = LayerMask.GetMask("WorldObstacles", "Enemies", "Players");

            this.detonationsCount = this.detonations.Length;
        }
        
        public override float GetSpeed()
        {
            return Speed;
        }

        public override void Activate(Vector3 pos, Quaternion rot)
        {
            if (!this.gameObject.activeSelf)
            {
                this.gameObject.SetActive(true);
            }

            if (this.TrOfThisCache.parent != null)
            {
                this.TrOfThisCache.SetParent(null);
            }

            this.TrOfThisCache.position = pos;
            this.TrOfThisCache.rotation = rot;

            this.timeOfDeactivate = Time.time + this.Lifetime;

            this.ray.origin = pos;
        }
        
        public override void Deactivate()
        {
        }
        
        void Update()
        {
            // Move
            Vector3 pos = this.ray.origin;
            pos += Time.deltaTime * this.Speed * this.TrOfThisCache.forward;
            this.TrOfThisCache.position = pos;

            // Collision detection
            Vector3 dir = pos - this.ray.origin;
            this.ray.direction = dir;
            var hits = Physics.RaycastAll(this.ray, dir.magnitude, targetsLayers);
            int count = hits.Length;

            if(count > 0)
            {
                RaycastHit hit = Utils.Physics.GetClosestHit(hits);

                IDamageReceiver damageReceiver = hit.collider.transform.gameObject.GetComponent<IDamageReceiver>();
                if(damageReceiver != null)
                { // Deal damage
                    damageReceiver.ReceiveDamage(this.Damage, hit.point);
                }
                else 
                { // Default effect

                }

                // Disable projectile
                this.gameObject.SetActive(false);

                // Play detonation effect
                for (int i = 0; i < this.detonationsCount; i++)
                {
                    ParticleSystem ps = this.detonations[i];
                    ps.transform.SetParent(null);
                    ps.transform.position = hit.point;
                    ps.transform.rotation = Quaternion.LookRotation(hit.normal, Vector3.up);
                    ps.Play();
                }
            }

            // Last pos
            this.ray.origin = pos;

            // Lifetime
            if (Time.time > this.timeOfDeactivate)
            {
                this.gameObject.SetActive(false);
            }
        }
    }
}