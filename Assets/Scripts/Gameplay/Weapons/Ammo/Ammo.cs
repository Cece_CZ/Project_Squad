using UnityEngine;

namespace Gameplay.Weapons.Ammo
{
    public class Ammo : MonoBehaviour, IAmmo
    {
        // Settings
        public Damage Damage;
        
        // Caches
        protected Transform TrOfThisCache;
        
        protected virtual void Awake()
        {
            this.TrOfThisCache = transform;
        }
        
        public virtual float GetSpeed()
        {
            return 0;
        }

        public virtual void Activate(Vector3 pos, Quaternion rot)
        {
        }

        public virtual void Deactivate()
        {
        }
    }
}
