using UnityEngine;

namespace Gameplay.Weapons.Ammo
{
    public class Beam : Ammo
    {
        // Settings
        public float Lifetime = 10;
        public float ActivationDuration = 0.05f;
        public float DisappearanceDuration = 0.3f;
        public float Range = 500;
        
        // State
        private float timeOfBirth;
        private Ray ray = new Ray();
        private float currentState = 0;
        private float targetState = 0;
        private int targetsLayers;

        private LineRenderer lineRenderer;
        
        protected override void Awake()
        {
            base.Awake();
            
            this.targetsLayers = LayerMask.GetMask("WorldObstacles", "Enemies", "Players");
            this.lineRenderer = GetComponentInChildren<LineRenderer>();
            this.lineRenderer.startWidth = 0;
            this.lineRenderer.endWidth = 0;
        }

        public override float GetSpeed()
        {
            return float.MaxValue;
        }

        public override void Activate(Vector3 pos, Quaternion rot)
        {
            if (!this.gameObject.activeSelf)
            {
                this.gameObject.SetActive(true);     
            }
            this.targetState = 1;
        }

        public override void Deactivate()
        {
            this.targetState = 0;
        }

        void Update()
        {
            if (this.targetState == 1)
            { // Activated
                if (this.currentState != this.targetState)
                {
                    this.currentState += (1 / this.ActivationDuration) * Time.deltaTime;

                    if(this.currentState > 1)
                    {
                        this.currentState = 1;
                    }
                }
            }
            else
            { // Deactivating
                this.currentState -= (1 / this.DisappearanceDuration) * Time.deltaTime;
                if(this.currentState < 0)
                {
                    this.currentState = 0;
                    this.gameObject.SetActive(false);
                    return;
                }    
            }

            this.ray.origin = this.TrOfThisCache.position;
            this.ray.direction = this.TrOfThisCache.forward;

            var hits = Physics.RaycastAll(this.ray, this.Range, targetsLayers);
            int count = hits.Length;

            if (count > 0)
            {
                RaycastHit finalHit = Utils.Physics.GetClosestHit(hits);

                IDamageReceiver damageReceiver = finalHit.collider.transform.gameObject.GetComponent<IDamageReceiver>();
                if (damageReceiver != null && this.currentState > 0.7)
                { // Deal damage
                    damageReceiver.ReceiveDamage(this.Damage, finalHit.point);
                }
                else
                { // Default effect

                }

                GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                Transform tr = sphere.transform;
                tr.position = finalHit.point;
                tr.localScale = new Vector3(0.1f, 0.1f, 0.1f) * this.currentState;

                sphere.transform.SetParent(finalHit.transform, true);

                // Laser beam shortage
                this.lineRenderer.SetPosition(1, new Vector3(0, 0, (finalHit.point - this.TrOfThisCache.position).magnitude));
            }

            this.lineRenderer.startWidth = 0.1f * this.currentState;
            this.lineRenderer.endWidth = 0.1f * this.currentState;
        }
    }
}
