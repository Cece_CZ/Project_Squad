using System.Collections;
using System.Collections.Generic;
using Gameplay.Weapons;
using UnityEngine;
using UnityEngine.Serialization;

public class Weapon_OneShot : Weapon 
{
    // State
    private int nextAmmunitionIndex = 0;
    
    // Debug
    LineRenderer lineRenderer;

    protected override void Awake()
    {
        base.Awake();
        
        // Debug
        this.lineRenderer = this.gameObject.AddComponent<LineRenderer>();
        this.lineRenderer.enabled = true;
        this.lineRenderer.startWidth = 0.01f;
        this.lineRenderer.endWidth = 0.01f;
        this.lineRenderer.useWorldSpace = true;
    }

    public override void SetTargeting(Ray ray)
    {
        if (!base.isDestroyed)
        {
            Vector3 finalTargetPos = ray.GetPoint(1000);
            if (
                Physics.Raycast(ray, out RaycastHit hit, 1000, TargetsLayers)
                )
            {
                finalTargetPos = hit.point;
            }

            this.lineRenderer.SetPosition(0, this.TrOfThisCache.position);
            this.lineRenderer.SetPosition(1, finalTargetPos);

            this.TrOfThisCache.LookAt(finalTargetPos, Vector3.up);
        }
    }

    public override void StartShooting()
    {
        if (!base.isDestroyed && ProjectilesCount > 0)
        {
            Projectiles[this.nextAmmunitionIndex].Activate(TrFirePoint.position, TrFirePoint.rotation);

            this.nextAmmunitionIndex = ++nextAmmunitionIndex % ProjectilesCount;
        }
    }

    public override void StopShooting()
    {
        for (int i = 0; i < ProjectilesCount; i++)
        {
            Projectiles[i].Deactivate();
        }
    }
}
