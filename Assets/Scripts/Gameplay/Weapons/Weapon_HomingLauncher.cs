using System.Collections;
using System.Collections.Generic;
using Gameplay.Weapons;
using UnityEngine;
using UnityEngine.Serialization;

public class Weapon_HomingLauncher : Weapon
{
    // Caches
    private Transform trTarget = null;
    
    // State
    private int index = 0;
    
    protected override void Awake()
    {
        base.Awake();
        
        GameObject target = new GameObject(this.name + "_Target");
        this.trTarget = target.transform;
    }

    public override void SetTargeting(Ray ray)
    {
        if (!base.isDestroyed)
        {
            Vector3 finalTargetPos = ray.GetPoint(1000);
            if (
                Physics.Raycast(ray, out RaycastHit hit, 1000, TargetsLayers)
                )
            {
                finalTargetPos = hit.point;
            }

            this.trTarget.position = finalTargetPos;
        }
    }

    public override void StartShooting()
    {
        if (!base.isDestroyed && ProjectilesCount > 0)
        {
            IAmmo ammo = Projectiles[this.index];

            IHoming homing = ammo as IHoming;
            if(homing != null)
            {
                homing.SetTarget(trTarget);
            }

            ammo.Activate(this.TrFirePoint.position, TrFirePoint.rotation);

            this.index = ++index % ProjectilesCount;
        }
    }

    public override void StopShooting()
    {

    }
}
