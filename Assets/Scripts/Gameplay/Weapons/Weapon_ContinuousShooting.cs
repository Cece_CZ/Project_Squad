using System.Collections;
using System.Collections.Generic;
using Gameplay.Weapons;
using UnityEngine;
using UnityEngine.Serialization;

public class Weapon_ContinuousShooting : Weapon
{
    // Settings
    public float FireRate = 100;
    private float firePeriode;

    // State
    private int nextAmmunitionIndex = 0;
    private float projectileSpeed = 0;
    private bool isShooting = false;
    private float timeOfLastProjectile = 0;
    private Vector3 lastFirepointPos;
    private Vector3 lastFirepointDir;

    protected override void Awake()
    {
        base.Awake();
        
        this.firePeriode = 1 / this.FireRate;

        if(Projectiles != null && ProjectilesCount > 0)
        {
            this.projectileSpeed = Projectiles[0].GetSpeed();
        }
    }

    void Update()
    {
        if(this.isShooting && !base.isDestroyed)
        {
            float time = Time.time;

            float creationTime = this.timeOfLastProjectile;
            Vector3 firepointPos = TrFirePoint.position;
            Vector3 firepointDir = TrFirePoint.forward;

            while (creationTime < time)
            {
                float currentLifetime = time - creationTime; 
                float state = (creationTime - this.timeOfLastProjectile) / (time - this.timeOfLastProjectile);

                // Release ammo
                if (ProjectilesCount > 0)
                {
                    Vector3 tempDir = Vector3.Lerp(this.lastFirepointDir, firepointDir, state);

                    Projectiles[this.nextAmmunitionIndex].Activate(
                         currentLifetime * this.projectileSpeed * tempDir + 
                         Vector3.Lerp(this.lastFirepointPos, firepointPos, state),

                         Quaternion.LookRotation(tempDir, Vector3.up));

                    this.nextAmmunitionIndex = ++nextAmmunitionIndex % ProjectilesCount;
                }
                creationTime += this.firePeriode;
            }

            this.timeOfLastProjectile = creationTime;
            this.lastFirepointPos = firepointPos;
            this.lastFirepointDir = firepointDir;
        }   
    }

    public override void SetTargeting(Ray ray)
    {
        if (!base.isDestroyed)
        {
            Vector3 finalTargetPos = ray.GetPoint(1000);
            if (Physics.Raycast(ray, out RaycastHit hit, 1000, TargetsLayers))
            {
                finalTargetPos = hit.point;
            }

            this.TrOfThisCache.LookAt(finalTargetPos, Vector3.up);
        }
    }

    public override void StartShooting()
    {
        if (!base.isDestroyed)
        {
            this.isShooting = true;
            this.timeOfLastProjectile = Time.time;

            this.lastFirepointPos = TrFirePoint.position;
            this.lastFirepointDir = TrFirePoint.forward;
        }
    }

    public override void StopShooting()
    {
        this.isShooting = false;
    }
}
