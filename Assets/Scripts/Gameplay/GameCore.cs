using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCore : MonoBehaviour
{
    // References
    public CanvasGame GameCanvas;
    private PlayersRobot[] robots;

    // State
    private bool isBulletTime = false;
    private bool isBulletTimeChanging = false;

    private void Awake()
    {
        this.robots = FindObjectsOfType<PlayersRobot>();
    }

    public void OnRobotActivateControl(PlayersRobot robot)
    {
        robot.IsControlActive = true;
    }
    public void OnRobotDeactivateControl(PlayersRobot robot)
    {
        robot.IsControlActive = false;
    }

    #region Processes

    public void ProcessMousePos(Vector3 pos)
    {
        int count = this.robots.Length;
        for (int i = 0; i < count; i++)
        {
            PlayersRobot pr = this.robots[i];
            if (pr.IsControlActive && pr.Weapon != null)
            {
                Ray ray = pr.Camera.ScreenPointToRay(pos);

                pr.Weapon.SetTargeting(ray);
            }
        }
    }
    public void ProcessStartShooting()
    {
        int count = this.robots.Length;
        for (int i = 0; i < count; i++)
        {
            PlayersRobot pr = this.robots[i];
            if (pr.IsControlActive && pr.Weapon != null)
            {
                pr.Weapon.StartShooting();
            }
        }
    }
    public void ProcessStopShooting()
    {
        int count = this.robots.Length;
        for (int i = 0; i < count; i++)
        {
            PlayersRobot pr = this.robots[i];
            if (pr.IsControlActive && pr.Weapon != null)
            {
                pr.Weapon.StopShooting();
            }
        }
    }
    public void ProcessInputForward()
    {
        int count = this.robots.Length;
        for (int i = 0; i < count; i++)
        {
            PlayersRobot pr = this.robots[i];
            if(pr.IsControlActive)
            {
                pr.Entity.StepForward();
            }
        }
    }
    public void ProcessInputBackward() 
    {
        int count = this.robots.Length;
        for (int i = 0; i < count; i++)
        {
            PlayersRobot pr = this.robots[i];
            if (pr.IsControlActive)
            {
                pr.Entity.StepBackward();
            }
        }
    }
    public void ProcessInputLeft()
    {
        int count = this.robots.Length;
        for (int i = 0; i < count; i++)
        {
            PlayersRobot pr = this.robots[i];
            if (pr.IsControlActive)
            {
                pr.Entity.StepLeft();
            }
        }
    }
    public void ProcessInputRight()
    {
        int count = this.robots.Length;
        for (int i = 0; i < count; i++)
        {
            PlayersRobot pr = this.robots[i];
            if (pr.IsControlActive)
            {
                pr.Entity.StepRight();
            }
        }

    }
    public void ProcessInputRotateLeft()
    {
        int count = this.robots.Length;
        for (int i = 0; i < count; i++)
        {
            PlayersRobot pr = this.robots[i];
            if (pr.IsControlActive)
            {
                pr.Entity.Rotate90Left();
            }
        }
    }
    public void ProcessInputRotateRight()
    {
        int count = this.robots.Length;
        for (int i = 0; i < count; i++)
        {
            PlayersRobot pr = this.robots[i];
            if (pr.IsControlActive)
            {
                pr.Entity.Rotate90Right();
            }
        }
    }
    public void ProcessInputTimeToCommand()
    {
        if(!this.isBulletTimeChanging)
        {
            this.isBulletTimeChanging = true;
            if(this.isBulletTime)
            {
                this.isBulletTime = false;
                StartCoroutine(BulletTimeTransition(GameSettings.Instance.BulletTime.Out));
            }
            else
            {
                this.isBulletTime = true;
                StartCoroutine(BulletTimeTransition(GameSettings.Instance.BulletTime.In));
            }
        }
    }

    #endregion

    #region Coroutines

    IEnumerator BulletTimeTransition(AnimationCurve animationCurve)
    {
        float startTime = Time.unscaledTime;
        float length = animationCurve.keys[animationCurve.length - 1].time;
        float currentTime;

        do
        {
            currentTime = Time.unscaledTime - startTime;

            Time.timeScale = animationCurve.Evaluate(currentTime);
            Time.fixedDeltaTime = 0.02f * Time.timeScale;
            yield return null;
        }
        while (currentTime <= length);
                  
        this.isBulletTimeChanging = false;
    }

    #endregion
}
