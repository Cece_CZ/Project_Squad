using Gameplay.Interfaces;
using UnityEngine;

public class MovingPath : MonoBehaviour, IGridAlignable
{
    public Vector3[] Waypoints = null;
    public void DoGridAlign()
    {
        int count = Waypoints.Length;
        for (int i = 0; i < count; i++)
        {
            Waypoints[i] = Utils.Scene.Functions.Vector3Align(Waypoints[i]);
        }
    }
}
