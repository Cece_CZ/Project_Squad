using System;
using Gameplay.Interfaces;
using UnityEngine;

namespace Gameplay.Levels
{
    [RequireComponent(typeof(Collider))]
    [RequireComponent(typeof(Rigidbody))]
    public class Activator : MonoBehaviour, IGridAlignable
    {
        public void DoGridAlign()
        {
            Utils.Scene.Functions.GridAlign(this.transform);
        }
        
        private void OnTriggerEnter(Collider other)
        {
            Debug.Log("Collision detected with " + other.gameObject.name);
        }
    }
}
