using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MovingPath)), CanEditMultipleObjects]
public class MovingPathEditor : Editor
{
    protected virtual void OnSceneGUI()
    {
        MovingPath movingPath = (MovingPath)target;
        Vector3[] Waypoints = movingPath.Waypoints;
        if (Waypoints != null)
        {
            int count = Waypoints.Length;
            int lastIndex = count - 1;
            if (count > 0)
            {
                Handles.color = Color.magenta;

                Handles.DrawPolyLine(Waypoints);
                for (int i = 0; i < count; ++i)
                {
                    Vector3 firstPoint = Waypoints[i];
                    Handles.DrawWireCube(firstPoint, Vector3.one * 0.1f);
                    Handles.Label(firstPoint, "           " + i.ToString("G"));

                    EditorGUI.BeginChangeCheck();
                    Vector3 newWaypointPosition = Handles.PositionHandle(firstPoint, Quaternion.identity);
                    if (EditorGUI.EndChangeCheck())
                    {
                        Undo.RecordObject(movingPath, "Change waypoint position");
                        Waypoints[i] = newWaypointPosition;
                    }
                    
                    // Drawing nearest points
                    // if (i < (count - 1))
                    // {
                    //     Vector3 secondPoint = Waypoints[i + 1];
                    //     Vector3 closestPoint = Utils.Polylines.FindClosestPointOnSegment(firstPoint, secondPoint, movingPath.transform.position);
                    //     Handles.DrawWireCube(closestPoint, Vector3.one * 0.3f);
                    // }
                }
                Handles.DrawDottedLine(Waypoints[lastIndex], Waypoints[0], 10); 
                
                // Drawing nearest point
                // Vector3 closestPoint = Utils.Polylines.GetNextPointFromClosestPoint(Waypoints, movingPath.transform.position, -1, true);
                // Handles.DrawWireCube(closestPoint, Vector3.one * 0.3f);
            }
        }
    }
    
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        
        MovingPath pointController = (MovingPath)target;
        
        if (Selection.activeGameObject == pointController.gameObject)
        {
            if (GUILayout.Button("Align"))
            {
                Debug.Log("Button clicked!");
            }
        }
    }
}