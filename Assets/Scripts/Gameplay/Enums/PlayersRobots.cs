namespace Enums
{
    /// <summary>
    /// Players robots types
    /// 0 - Top left view
    /// 1 - Top right view
    /// 2 - Bottom left view
    /// 3 - Bottom right view
    /// </summary>
    public enum PlayersRobot
    {
        Robot0,
        Robot1,
        Robot2,
        Robot3,
    }
}

