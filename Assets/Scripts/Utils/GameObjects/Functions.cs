using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utils.GameObjects
{
    public class Functions
    {
        public static Collider[] GetCollidersInLayer(GameObject go, string layerName)
        {
            List<Collider> colliders = new List<Collider>(1);
            int layerToFind = LayerMask.NameToLayer(layerName);
            var tempColliders = go.GetComponentsInChildren<Collider>();
            foreach (Collider c in tempColliders)
            {
                if (c.gameObject.layer == layerToFind)
                {
                    colliders.Add(c);
                }
            }
            return colliders.ToArray();
        }

        public static Collider[] GetCollidersInLayer(GameObject go, params string[] layerName)
        {
            List<Collider> colliders = new List<Collider>(1);
            int layersToFind = LayerMask.GetMask(layerName);
            var tempColliders = go.GetComponentsInChildren<Collider>();
            foreach (Collider c in tempColliders)
            {
                if (((1 << c.gameObject.layer) & layersToFind) > 0)
                {
                    colliders.Add(c);
                }
            }
            return colliders.ToArray();
        }

        public static Collider GetColliderInLayer(GameObject go, string layerName)
        {
            int layerToFind = LayerMask.NameToLayer(layerName);
            var tempColliders = go.GetComponentsInChildren<Collider>();
            foreach (Collider c in tempColliders)
            {
                if (c.gameObject.layer == layerToFind)
                {
                    return c;
                }
            }
            return null;
        }

    }
}
