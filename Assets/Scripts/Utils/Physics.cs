using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utils
{
    public class Physics
    {
        /// <summary>
        /// Return closest hitpoint (because unity do not do that)
        /// </summary>
        /// <param name="hits">Array of hits</param>
        /// <returns>Closest hitpoint</returns>
        public static RaycastHit GetClosestHit(RaycastHit[] hits)
        {
            int count = hits.Length;
            int finalIndex = 0;

            float closest = hits[finalIndex].distance;

            for (int i = 1; i < count; i++)
            {
                float distance = hits[i].distance;
                if (distance < closest)
                {
                    closest = distance;
                    finalIndex = i;
                }
            }

            return hits[finalIndex];
        }
    }
}
