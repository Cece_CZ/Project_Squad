using UnityEngine;

namespace Utils.Scene
{
    public class Functions : MonoBehaviour
    {
        /// <summary>
        /// Returns GameObjects path in hierarchy
        /// Recursive
        /// </summary>
        public static string GetGameObjectPath(Transform tr)
        {
            if(tr == null) // End
            {
                return "";
            }
            else // Go up
            {
                return GetGameObjectPath(tr.parent) + "/" + tr.name;
            }
        }

        public static void GridAlign(Transform transform)
        {
            transform.position = Vector3Align(transform.position);
        }

        public static Vector3 Vector3Align(Vector3 vector)
        {
            Vector3 value;
            value.x = Mathf.RoundToInt(vector.x);
            value.y = Mathf.RoundToInt(vector.y);
            value.z = Mathf.RoundToInt(vector.z);

            return value;
        }
    }
}
