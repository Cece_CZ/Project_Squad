using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Utils.Scene;

public class MenuPrintAllGOInLayer
{
    const string layerName = "WorldObstacles";
    // Add a menu item named "Do Something" to MyMenu in the menu bar.
    [MenuItem("Cece/Print [" + layerName + "] layer")]
    static void Print()
    {
        int layer = LayerMask.NameToLayer(layerName);

        var gos = MonoBehaviour.FindObjectsOfType<GameObject>(true);
        
        foreach (GameObject go in gos)
        {
            if(go.layer == layer)
            {
                Debug.Log(Functions.GetGameObjectPath(go.transform));
            }
        }
    }
}
