using System;
using System.Collections;
using System.Collections.Generic;
using Gameplay.Interfaces;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

public class MenuAlignLevel
{ 
    [MenuItem("Cece/Align level")]
    static void Print()
    {
        var allGameObjects = Object.FindObjectsOfType<MonoBehaviour>();
        int countObjects = allGameObjects.Length;
        for (int i = 0; i < countObjects; i++)
        {
            IGridAlignable gridAlignable = allGameObjects[i].GetComponent<IGridAlignable>();
            if (gridAlignable != null)
            {
                gridAlignable.DoGridAlign();
            }
        }
    }
}
