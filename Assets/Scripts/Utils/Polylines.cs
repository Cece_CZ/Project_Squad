﻿using UnityEngine;

namespace Utils
{
    public class Polylines
    {
        public static Vector3 GetClosestPointOnSegment(Vector3 segmentStart, Vector3 segmentEnd, Vector3 point)
        {
            Vector3 segmentDirection = segmentEnd - segmentStart;
            
            Vector3 pointToSegmentStart = point - segmentStart;

            float segmentLength = segmentDirection.magnitude;
            
            segmentDirection.Normalize();

            float dotProduct = Vector3.Dot(pointToSegmentStart, segmentDirection);
            dotProduct = Mathf.Clamp(dotProduct, 0f, segmentLength);
            
            Vector3 closestPointOnSegment = segmentStart + segmentDirection * dotProduct;

            return closestPointOnSegment;
        }

        public static Vector3 GetNextPointFromClosestPoint(Vector3[] polyline, Vector3 startingPoint, float distanceFromStartingPoint, bool loop = false)
        {
            Vector3 closestPoint = Vector3.zero;
            float closestDistance = Mathf.Infinity;
            int count = polyline.Length;
            int lastIndex = count - 1; // Also last index
            int segmentIndex = -2;
            
            // Search through basic polyline
            for (int i = 0; i < lastIndex; i++)
            {
                var segmentPoint = GetClosestPointOnSegment(polyline[i], polyline[i + 1], startingPoint);
                float distance = Vector3.Distance(startingPoint, segmentPoint);
                
                if (distance < closestDistance)
                {
                    closestPoint = segmentPoint;
                    closestDistance = distance;
                    segmentIndex = i;
                }
            }

            // Last point should be connected to first
            if (loop)
            {
                var segmentPoint = GetClosestPointOnSegment(polyline[lastIndex], polyline[0], startingPoint);
                float distance = Vector3.Distance(startingPoint, segmentPoint);
                
                if (distance < closestDistance)
                {
                    closestPoint = segmentPoint;
                    segmentIndex = lastIndex;
                }
            }
            
            Vector3 firstPoint = closestPoint;
            
            // Try to resolve point along the way
            if (distanceFromStartingPoint > 0) // Continue in path
            {
                while(true)
                {
                    ++segmentIndex;
                    if (segmentIndex > lastIndex) // Loop check
                    {
                        segmentIndex = 0;
                    }
                    Vector3 secondPoint = polyline[segmentIndex];
                    float distanceToRemove = Vector3.Distance(firstPoint, secondPoint);

                    if (distanceFromStartingPoint > distanceToRemove) // Short distance and continue
                    {
                        distanceFromStartingPoint -= distanceToRemove;
                    }
                    else // Point is in this segment
                    {
                        return firstPoint + (secondPoint - firstPoint).normalized * distanceFromStartingPoint;
                    }

                    // At this point if not loop just check if last index
                    if (!loop && segmentIndex == lastIndex)
                    {
                        return polyline[lastIndex];
                    }

                    firstPoint = secondPoint;
                } 
            }
            else // Going backwards
            {
                // Back to normal distance
                distanceFromStartingPoint *= -1;
                while(true)
                {
                    Vector3 secondPoint = polyline[segmentIndex];
                    float distanceToRemove = Vector3.Distance(firstPoint, secondPoint);

                    if (distanceFromStartingPoint > distanceToRemove) // Short distance and continue
                    {
                        distanceFromStartingPoint -= distanceToRemove;
                    }
                    else // Point is in this segment
                    {
                        return firstPoint + (secondPoint - firstPoint).normalized * distanceFromStartingPoint;
                    }

                    // At this point if not loop just check if first index
                    if (!loop && segmentIndex == 0)
                    {
                        return polyline[0];
                    }

                    --segmentIndex;
                    if (segmentIndex < 0)
                    {
                        segmentIndex = lastIndex;
                    }
                    firstPoint = secondPoint;
                } 
            }

            return Vector3.zero;
        }
    }
}