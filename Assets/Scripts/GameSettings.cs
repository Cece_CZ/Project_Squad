using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSettings : MonoBehaviour
{
    public float GridSize = 1;

    public BulletTime BulletTime;

    #region Singleton

    static GameSettings instance = null;
    public static GameSettings Instance
    {
        get
        {
            return instance;
        }
    }
    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(this.gameObject);

        //Application.targetFrameRate = -1; 
    }

    #endregion
}
