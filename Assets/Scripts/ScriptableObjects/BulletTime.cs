using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BulletTime", menuName = "ScriptableObjects/BulletTime", order = 1)]
public class BulletTime
    : ScriptableObject
{
    public AnimationCurve In = new AnimationCurve();
    public AnimationCurve Out = new AnimationCurve();
}