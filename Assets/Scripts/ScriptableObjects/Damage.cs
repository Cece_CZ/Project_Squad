using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Damage", menuName = "ScriptableObjects/Damage", order = 1)]
public class Damage : ScriptableObject, IDamage
{
    public DamageType DamageType;
    public DamageType GetDamageType => DamageType;

    public float Amount;
    public float GetAmount => Amount;
}