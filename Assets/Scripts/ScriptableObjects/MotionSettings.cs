using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MotionSettings", menuName = "ScriptableObjects/EntityMotionSettings", order = 1)]
public class MotionSettings : ScriptableObject
{
    public AnimationCurve StepCurve = new AnimationCurve();

    public AnimationCurve RotateCurve = new AnimationCurve();
}