using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerInput : MonoBehaviour
{
    // References
    private GameCore gameCore;

    void Awake()
    {
        this.gameCore = FindObjectOfType<GameCore>();
    }

    void Update()
    {
        this.gameCore.ProcessMousePos(Input.mousePosition);

        if(Input.GetMouseButtonDown(0))
        {
            this.gameCore.ProcessStartShooting();
        }

        if (Input.GetMouseButtonUp(0))
        {
            this.gameCore.ProcessStopShooting();
        }

        if (Input.GetKey(KeyCode.W))
        {
            this.gameCore.ProcessInputForward();
        }

        if (Input.GetKey(KeyCode.S))
        {
            this.gameCore.ProcessInputBackward();
        }

        if (Input.GetKey(KeyCode.A))
        {
            this.gameCore.ProcessInputLeft();
        }

        if (Input.GetKey(KeyCode.D))
        {
            this.gameCore.ProcessInputRight();
        }

        if (Input.GetKey(KeyCode.Q))
        {
            this.gameCore.ProcessInputRotateLeft();
        }

        if (Input.GetKey(KeyCode.E))
        {
            this.gameCore.ProcessInputRotateRight();
        }

        if(Input.GetKeyDown(KeyCode.Space))
        {
            this.gameCore.ProcessInputTimeToCommand();
        }

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        if (Input.GetKeyDown(KeyCode.F5))
        {
            Scene scene = SceneManager.GetActiveScene(); 
            SceneManager.LoadScene(scene.name);
        }
    }
}
