using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI
{
    public class Robot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private GameCore gameCore;
        [SerializeField] private GameObject destroyedUI;

        public PlayersRobot RobotInstance;

        void Awake()
        {
            destroyedUI.SetActive(false);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            this.gameCore.OnRobotActivateControl(this.RobotInstance);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            this.gameCore.OnRobotDeactivateControl(this.RobotInstance);
        }

        public void ShowDestroy()
        {
            destroyedUI.SetActive(true);
        }
    }
}