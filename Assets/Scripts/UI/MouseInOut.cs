using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UI
{
    public class MouseInOut : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public void OnPointerEnter(PointerEventData eventData)
        {
            Debug.Log("Enter " + this.name);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            Debug.Log("Exit " + this.name);
        }
    }
}
