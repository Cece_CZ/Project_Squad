﻿using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;

namespace UI.Viewports
{
    // Main purpose is control camera Rect according UI change of the Panel with this component
    public class PanelCameraViewport : MonoBehaviour
    {
        [SerializeField]
        private int cameraID = 0;
        public int CameraID => cameraID;

        private Canvas mainCanvas;
        
        private RectTransform trCanvas;
        private RectTransform trPanel;
        
        private CameraController cameraController = null;
        private bool isCameraControllerConnected = false;
        
        private void Awake()
        {
            this.mainCanvas = GetComponentInParent<Canvas>();
            this.trCanvas = mainCanvas.GetComponent<RectTransform>();
            this.trPanel = GetComponent<RectTransform>();
        }

        private void Update()
        {
            if (!isCameraControllerConnected)
            {
                return;
            }
            
            // TODO: 2023-08-06 - Check if there are some changes in screen size and panel
            // Rect for camera
            Vector2 offsetMin = trPanel.offsetMin;
            Vector2 offsetMax = trPanel.offsetMax;
            
            Vector3 minInScreen = trCanvas.TransformPoint(new Vector3(offsetMin.x, offsetMin.y, 0));
            Vector3 maxInScreen = trCanvas.TransformPoint(new Vector3(offsetMax.x, offsetMax.y, 0));
            
            Rect newRect =
                new Rect(
                    minInScreen.x / (Screen.width),
                    minInScreen.y / (Screen.height),
                    (maxInScreen.x - minInScreen.x) / (Screen.width),
                    (maxInScreen.y - minInScreen.y) / (Screen.height)
                );

            cameraController.SetRect(newRect);
        }
        
        public void RegisterCameraController(CameraController cameraControllerToSet)
        {
            isCameraControllerConnected = (this.cameraController = cameraControllerToSet) != null;
        }
    }
}