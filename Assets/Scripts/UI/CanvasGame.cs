using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasGame : MonoBehaviour
{
    RectTransform rectTransform;

    private int resolutionX;
    private int resolutionY;

    UI.Robot[] robotsUIs;

    private void Awake()
    {
        this.rectTransform = this.GetComponent<RectTransform>();
        this.robotsUIs = this.GetComponentsInChildren<UI.Robot>();
    }

    void Start()
    {
        this.resolutionX = Screen.width;
        this.resolutionY = Screen.height;

        this.UpdateCamerasRect();
    }

    // Update is called once per frame
    void Update()
    {
        float ratio = this.rectTransform.rect.width / this.rectTransform.rect.height;
        if(this.resolutionX != Screen.width || this.resolutionY != Screen.height)
        {
            this.resolutionX = Screen.width;
            this.resolutionY = Screen.height;

            Debug.Log("Ratio changed");

            this.UpdateCamerasRect();
        }
    }

    public void UpdateCamerasRect()
    {
        int count = this.robotsUIs.Length;
        for(int i = 0; i < count; i++)
        {
            UI.Robot r = this.robotsUIs[i];
            var canvasTr = this.rectTransform;
            var rTr = r.GetComponent<RectTransform>();

            // Find rect in screen position
            var pos0 = canvasTr.TransformPoint(new Vector3(rTr.offsetMin.x, rTr.offsetMin.y, 0));
            var pos1 = canvasTr.TransformPoint(new Vector3(rTr.offsetMax.x, rTr.offsetMax.y, 0));

            // Rect for camera
            Rect newRect =
                new Rect(
                    pos0.x / this.resolutionX,
                    pos0.y / this.resolutionY,
                    (pos1.x - pos0.x) / this.resolutionX,
                    (pos1.y - pos0.y) / this.resolutionY
                    );
            if (r.RobotInstance.Camera != null)
            {
                r.RobotInstance.Camera.rect = newRect;
            }
        }
    }
}
