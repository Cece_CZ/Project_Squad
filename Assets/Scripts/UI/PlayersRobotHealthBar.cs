using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI
{
    public class PlayersRobotHealthBar : MonoBehaviour, IHealthBar
    {
        public void Set(float state)
        {
            Vector3 scale = this.transform.localScale;
            scale.y = state;
            this.transform.localScale = scale;
        }
    }
}
