using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityStatusBillboard : MonoBehaviour
{
    // Caches
    private Transform trCache;
    private Transform trTargetCamera;

    void Awake()
    {
        this.trCache = this.transform;

        // Layers TODO: Optimize to global class
        int[] visionMasks = {
            LayerMask.GetMask("RobotVision0"),
            LayerMask.GetMask("RobotVision1"),
            LayerMask.GetMask("RobotVision2"),
            LayerMask.GetMask("RobotVision3")
        };

        // It counting on layers order
        // Finds vision mask
        int mask = visionMasks[this.gameObject.layer - LayerMask.NameToLayer("RobotVision0")];

        // Finding camera with right culling mask
        Camera[] cameras = Camera.allCameras;
        int count = cameras.Length;
        for (int i = 0; i < count; i++)
        {
            Camera cam = cameras[i];
            if((cam.cullingMask & mask) > 0)
            {
                this.trTargetCamera = cam.transform;
                break; // Found
            }
        }
    }

    void LateUpdate()
    {
        if (!this.trTargetCamera)
        {
            return;
        }
        this.trCache.LookAt(this.trCache.position + this.trTargetCamera.forward);
    }
}
