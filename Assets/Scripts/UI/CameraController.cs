using System;
using System.Collections;
using System.Collections.Generic;
using UI.Viewports;
using UnityEngine;
using UnityEngine.Serialization;

public class CameraController : MonoBehaviour
{
    public int CameraID;

    private Camera cameraCache;
    
    private void Awake()
    {
        this.cameraCache = GetComponent<Camera>();
    }

    private void Start()
    {
        PanelCameraViewport[] panels = GameObject.FindObjectsOfType<PanelCameraViewport>();
        foreach (PanelCameraViewport panel in panels)
        {
            if (CameraID == panel.CameraID)
            {
                panel.RegisterCameraController(this);
                break;
            }
        }
    }

    public void SetRect(Rect newRect)
    {
        this.cameraCache.rect = newRect;
    }
}
